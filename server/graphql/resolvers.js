const bcrypt = require('bcryptjs');
const { default: validator } = require('validator');
const jwt = require('jsonwebtoken');

const { clearImage } = require('../utils/file');
const messages = require('../utils/messages.json');
const {
  ForbiddenError,
  NotFoundError,
  UnauthorizedError,
  ValidationError,
} = require('../utils/errors');
const { secret } = require('../utils/secret.json');

// Models
const User = require('../models/user');
const Post = require('../models/post');

const resolvers = {
  login: async ({ email, password }) => {
    const user = await User.findOne({ email });
    if (!user) throw new UnauthorizedError(messages.emailDoesntExist);
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) throw new UnauthorizedError(messages.passwordIncorrect);
    const token = jwt.sign(
      { userId: user._id.toString(), email: user.email },
      secret,
      { expiresIn: '1h' }
    );
    return { token, userId: user._id.toString() };
  },

  getPosts: async ({ page }, req) => {
    if (!req.isAuth) throw new UnauthorizedError(messages.notAuth);
    if (!page) page = 1;
    const POSTS_PER_PAGE = 2;
    const totalPosts = await Post.find().countDocuments();
    const posts = await Post.find()
      .populate('creator')
      .sort({ createdAt: -1 })
      .skip((page - 1) * POSTS_PER_PAGE)
      .limit(POSTS_PER_PAGE);
    return {
      posts: posts.map((post) => {
        return {
          ...post._doc,
          _id: post._id.toString(),
          createdAt: post.createdAt.toISOString(),
          updatedAt: post.updatedAt.toISOString(),
        };
      }),
      totalPosts,
    };
  },

  getSinglePost: async ({ id }, req) => {
    if (!req.isAuth) throw new UnauthorizedError(messages.notAuth);
    const post = await Post.findById(id).populate('creator');
    if (!post) throw new NotFoundError(messages.postNotFound);
    return {
      ...post._doc,
      _id: post._id.toString(),
      createdAt: post.createdAt.toISOString(),
      updatedAt: post.updatedAt.toISOString(),
    };
  },

  getUser: async (args, req) => {
    if (!req.isAuth) throw new UnauthorizedError(messages.notAuth);
    const user = await User.findById(req.userId);
    if (!user) throw NotFoundError(messages.userNotFound);
    return { ...user._doc, _id: user._id.toString() };
  },

  createUser: async ({ userInput }, req) => {
    const { name, email, password } = userInput;
    // Validation
    const errors = [];
    if (
      validator.isEmpty(email) ||
      validator.isEmpty(password) ||
      validator.isEmpty(name)
    )
      errors.push({ msg: messages.emptyFields });
    if (!validator.isEmail(email)) errors.push({ msg: messages.emailInvalid });
    if (!validator.isLength(password, { min: 6 }))
      errors.push({ msg: messages.passwordTooShort });
    if (errors.length > 0) throw new ValidationError(errors[0].msg, errors);

    const existingUser = await User.findOne({ email });
    if (existingUser) throw new ValidationError(messages.emailExists);
    const hashedPasswrd = await bcrypt.hash(password, 12);
    const newUser = new User({ email, password: hashedPasswrd, name });
    const createdUser = await newUser.save();
    return { ...createdUser._doc, _id: createdUser._id.toString() };
  },

  createPost: async ({ postInput }, req) => {
    if (!req.isAuth) throw new UnauthorizedError(messages.notAuth);
    const { title, content, imgUrl } = postInput;
    const errors = [];
    if (validator.isEmpty(title) || validator.isEmpty(content))
      errors.push({ msg: messages.emptyFields });
    if (!validator.isLength(title, { min: 5 }))
      errors.push({ msg: messages.titleTooShort });
    if (!validator.isLength(content, { min: 5 }))
      errors.push({ msg: messages.contentTooShort });
    if (errors.length > 0) throw new ValidationError(errors[0].msg, errors);

    const user = await User.findById(req.userId);
    if (!user) throw new UnauthorizedError(messages.userNotFound);
    const newPost = new Post({
      title,
      content,
      imgUrl,
      creator: user,
    });
    const createdPost = await newPost.save();
    user.posts.push(createdPost);
    await user.save();
    return {
      ...createdPost._doc,
      _id: createdPost._id.toString(),
      createdAt: createdPost.createdAt.toISOString(),
      updatedAt: createdPost.updatedAt.toISOString(),
    };
  },

  editPost: async ({ id, postInput }, req) => {
    if (!req.isAuth) throw new UnauthorizedError(messages.notAuth);
    const post = await Post.findById(id).populate('creator');
    if (!post) throw new NotFoundError(messages.postNotFound);
    if (post.creator._id.toString() !== req.userId.toString())
      throw new ForbiddenError(messages.notAllowed);

    const { title, content, imgUrl } = postInput;
    const errors = [];
    if (validator.isEmpty(title) || validator.isEmpty(content))
      errors.push({ msg: messages.emptyFields });
    if (!validator.isLength(title, { min: 5 }))
      errors.push({ msg: messages.titleTooShort });
    if (!validator.isLength(content, { min: 5 }))
      errors.push({ msg: messages.contentTooShort });
    if (errors.length > 0) throw new ValidationError(errors[0].msg, errors);

    post.title = title;
    post.content = content;
    if (imgUrl !== 'undefined') post.imgUrl = imgUrl;
    const updatedPost = await post.save();
    return {
      ...updatedPost._doc,
      _id: updatedPost._id.toString(),
      createdAt: updatedPost.createdAt.toISOString(),
      updatedAt: updatedPost.updatedAt.toISOString(),
    };
  },

  deletePost: async ({ id }, req) => {
    if (!req.isAuth) throw new UnauthorizedError(messages.notAuth);
    const post = await Post.findById(id);
    if (!post) throw new NotFoundError(messages.postNotFound);
    if (post.creator.toString() !== req.userId.toString())
      throw new ForbiddenError(messages.notAllowed);

    clearImage(post.imgUrl);
    const deletedPost = await Post.findByIdAndDelete(id).populate('creator');
    const user = await User.findById(req.userId);
    user.posts.pull(id);
    await user.save();
    return {
      ...deletedPost._doc,
      _id: deletedPost._id.toString(),
      createdAt: deletedPost.createdAt.toISOString(),
      updatedAt: deletedPost.updatedAt.toISOString(),
    };
  },

  updateUserStatus: async ({ newStatus }, req) => {
    if (!req.isAuth) throw new UnauthorizedError(messages.notAuth);
    const user = await User.findById(req.userId);
    if (!user) throw new NotFoundError(messages.userNotFound);
    user.status = newStatus;
    const updatedUser = await user.save();
    return { ...updatedUser._doc, _id: updatedUser._id.toString() };
  },
};

module.exports = resolvers;
