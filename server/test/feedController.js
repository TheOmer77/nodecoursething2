const { expect } = require('chai');
const sinon = require('sinon');
const mongoose = require('mongoose');

const User = require('../models/user');
const Post = require('../models/post');
const FeedController = require('../controllers/feed');
const io = require('../utils/socket');
const { CustomError } = require('../utils/errors');
const messages = require('../utils/messages.json');

const MONGO_URI =
  'mongodb+srv://OmerChameleon:chameleon@cluster0.scd9o.mongodb.net/nodeThing2_testing?retryWrites=true&w=majority';
const DUMMY_USERID = '5c0f66b979af55031b34728a';

describe('Feed controller', () => {
  before(async () => {
    await mongoose.connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const user = new User({
      email: 'test@test.com',
      password: 'test12345',
      name: 'test',
      posts: [],
      _id: DUMMY_USERID,
    });
    await user.save();
  });

  beforeEach(async () => {});
  beforeEach(async () => {});

  after(async () => {
    await User.findByIdAndDelete(DUMMY_USERID);
    await mongoose.disconnect();
  });

  it('Should add a created post to the posts of its creator', async () => {
    // Do not test socket.io
    const ioStub = sinon.stub(io, 'emit');

    const req = {
      body: {
        title: 'Test Post',
        content: 'Test Test Test Test Test',
      },
      file: { path: 'images/someRandomImg.png' },
      userId: DUMMY_USERID,
    };
    const res = {
      status: function (code) {
        this.statusCode = code;
        return this;
      },
      json: function (data) {
        this.userStatus = data.status;
      },
    };

    const result = await FeedController.createPost(req, res, () => {});
    expect(result.creator).to.have.property('posts');
    expect(result.creator.posts).to.have.length(1);
    ioStub.restore();
  });
});
