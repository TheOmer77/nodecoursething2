const { expect } = require('chai');
const sinon = require('sinon');
const mongoose = require('mongoose');

const User = require('../models/user');
const AuthController = require('../controllers/auth');
const { CustomError, NotFoundError } = require('../utils/errors');
const messages = require('../utils/messages.json');

const MONGO_URI =
  'mongodb+srv://OmerChameleon:chameleon@cluster0.scd9o.mongodb.net/nodeThing2_testing?retryWrites=true&w=majority';
const DUMMY_USERID = '5c0f66b979af55031b34728a';

describe('Auth controller', () => {
  before(async () => {
    await mongoose.connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const user = new User({
      email: 'test@test.com',
      password: 'test12345',
      name: 'test',
      posts: [],
      _id: DUMMY_USERID,
    });
    await user.save();
  });

  beforeEach(async () => {});
  beforeEach(async () => {});

  after(async () => {
    await User.findByIdAndDelete(DUMMY_USERID);
    await mongoose.disconnect();
  });

  it('Should throw an error if accessing the database fails', async () => {
    // A regular error does not have any status code by default. A CustomError has a default status code of 500.
    const stub = sinon.stub(User, 'findOne').throws(new CustomError());

    const req = {
      body: {
        email: 'asd@sad.com',
        password: 'asdsad',
      },
    };

    const result = await AuthController.login(req, {}, () => {});
    expect(result).to.be.an('error');
    expect(result).to.have.property('statusCode', 500);

    stub.restore();
  });

  it('Should send a response with a valid user status with an existing user', async () => {
    const req = { userId: DUMMY_USERID };
    const res = {
      statusCode: 500,
      userStatus: null,
      status: function (code) {
        this.statusCode = code;
        return this;
      },
      json: function (data) {
        this.userStatus = data.status;
      },
    };

    await AuthController.getUserStatus(req, res, () => {});
    expect(res.statusCode).to.be.equal(200);
    expect(res.userStatus).to.be.equal(messages.defaultStatus);
  });
});
