const { expect } = require('chai');
const jwt = require('jsonwebtoken');
const { JsonWebTokenError } = require('jsonwebtoken');
const sinon = require('sinon');

const checkAuth = require('../middleware/auth');

const { UnauthorizedError } = require('../utils/errors');
const messages = require('../utils/messages.json');

describe('Auth middleware', () => {
  it('Should throw an error if no authorization header is present', () => {
    const req = {
      get: () => null,
    };

    // Expect an error of type UnauthorizedError
    expect(checkAuth.bind(this, req, {}, () => {})).to.throw(UnauthorizedError);
    // Expect an error with message "You are not authenticated."
    expect(checkAuth.bind(this, req, {}, () => {})).to.throw(messages.notAuth);
  });

  it('Should throw an error if the authorization header is only one string', () => {
    const req = {
      get: () => 'ThisShouldFail',
    };

    // Expect an error of type JsonWebTokenError
    expect(checkAuth.bind(this, req, {}, () => {})).to.throw(JsonWebTokenError);
  });

  it('Should throw an error if the token cannot be verified', () => {
    const req = {
      get: () => 'Bearer ThisShouldFail',
    };

    // Expect an error of type JsonWebTokenError
    expect(checkAuth.bind(this, req, {}, () => {})).to.throw(JsonWebTokenError);
  });

  it('Should yield a userId after decoding the token', () => {
    const req = {
      get: () => 'Bearer ThisShouldFail',
    };

    sinon.stub(jwt, 'verify');
    jwt.verify.returns({ userId: 'someUserId' });

    checkAuth(req, {}, () => {});
    expect(req).to.have.property('userId');
    expect(req).to.have.property('userId', 'someUserId');
    expect(jwt.verify.called).to.be.true;
    jwt.verify.restore();
  });
});
