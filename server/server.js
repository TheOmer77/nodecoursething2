const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const multer = require('multer');
const { graphqlHTTP } = require('express-graphql');

const cors = require('./middleware/cors');
const auth = require('./middleware/auth');

const { clearImage } = require('./utils/file');
const getDateString = require('./utils/getDateString');
const messages = require('./utils/messages.json');
const { UnauthorizedError } = require('./utils/errors');
const { graphqlSchema: getGraphqlSchema } = require('./utils/graphql');
const graphqlSchema = getGraphqlSchema();
const graphqlResolvers = require('./graphql/resolvers');

const PORT = process.env.PORT || 8080;
const MONGO_URI =
  'mongodb+srv://OmerChameleon:chameleon@cluster0.scd9o.mongodb.net/nodeThing2?retryWrites=true&w=majority';

// Express app
const app = express();

// Setup multer
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'images');
  },
  filename: (req, file, cb) => {
    cb(null, `${getDateString(new Date())}-${file.originalname}`);
  },
});
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

// Middleware
app.use(express.json());
app.use(cors);
app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single('img'));
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use(auth);

// GraphQL
app.use(
  '/graphql',
  graphqlHTTP({
    schema: graphqlSchema,
    rootValue: graphqlResolvers,
    graphiql: true,
    customFormatErrorFn: (err) => {
      if (!err.originalError) return err;
      const { data, statusCode } = err.originalError;
      const { message } = err || messages.defaultErrorMsg;
      return { message, statusCode, data };
    },
  })
);

// Image uploading
app.put('/postImg', (req, res, next) => {
  if (!req.isAuth) throw new UnauthorizedError(messages.notAuth);
  if (!req.file)
    return res.status(200).json({ msg: messages.imageNotProvided });
  if (req.body.oldPath) clearImage(req.body.oldPath);
  return res.status(201).json({
    msg: messages.fileStored,
    filePath: req.file.path.replace('\\', '/'),
  });
});

// Error handler
app.use((error, req, res, next) => {
  if (!error.statusCode) error.statusCode = 500;
  console.error(error);
  const { message, statusCode, data } = error;
  res.status(statusCode).json({ message, data });
});

// Message to display in the console once the server is running
const serverRunningMsg = () => {
  console.clear();
  process.stdout.write('\033c');
  console.log(`\x1b[42m\x1b[30mServer is listening on port ${PORT}.\x1b[0m`);
};

// Connect to DB, and once connected start the server
mongoose
  .connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => app.listen(PORT, serverRunningMsg))
  .catch((err) => console.error(err));
