const { body } = require('express-validator');
const messages = require('./messages.json');
const User = require('../models/user');

const TITLE_MIN_LENGTH = 5;
const CONTENT_MIN_LENGTH = 5;
const MIN_PASSWORD_LENGTH = 6;

const validatePost = [
  body('title', messages.titleTooShort)
    .trim()
    .isLength({ min: TITLE_MIN_LENGTH }),
  body('content', messages.contentTooShort)
    .trim()
    .isLength({ min: CONTENT_MIN_LENGTH }),
];

const validateLogin = [
  body(['email', 'password'], messages.emptyFields).not().isEmpty(),
  body('email').isEmail().withMessage(messages.emailInvalid).normalizeEmail(),
];

const validateSignup = [
  body(['email', 'password', 'name'], messages.emptyFields).not().isEmpty(),
  body('email')
    .isEmail()
    .withMessage(messages.emailInvalid)
    .custom(async (value, { req }) => {
      const user = await User.findOne({ email: value });
      if (user) return Promise.reject(messages.emailExists);
    })
    .normalizeEmail(),
  body('password', messages.passwordTooShort)
    .trim()
    .isLength(MIN_PASSWORD_LENGTH),
  body('name').trim(),
];

validateStatus = [body('status').trim().not().isEmpty()];

module.exports = {
  validatePost,
  validateLogin,
  validateSignup,
  validateStatus,
};
