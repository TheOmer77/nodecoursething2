const { join } = require('path');
const { loadSchemaSync } = require('@graphql-tools/load');
const { GraphQLFileLoader } = require('@graphql-tools/graphql-file-loader');

const graphqlSchema = () => {
  return loadSchemaSync(join(__dirname, '../graphql/schema.graphql'), {
    loaders: [new GraphQLFileLoader()],
  });
};

module.exports = { graphqlSchema };
