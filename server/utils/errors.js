const messages = require('./messages.json');

class CustomError extends Error {
  /**
   * This is a base for custom errors.
   * @param {String} message The error message.
   * @param {String} name The error name. 'Error' by default.
   * @param {Number} [statusCode] The HTTP status code for the error type. 500 by default.
   * @param {*} [data] Additional data to display with the error.
   */
  constructor(
    message = messages.defaultErrorMsg,
    errorName = 'Error',
    statusCode = 500,
    data
  ) {
    super(message);
    if (data) this.data = data;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError);
    }

    this.name = errorName;
    this.statusCode = statusCode;
  }
}

class UnauthorizedError extends CustomError {
  /**
   * This error should be thrown whenever a client tries to access a resource, but is unable to do so due to being unauthorized. Status code: 401
   * @param {String} message The error message.
   * @param {*} [data] Additional data to display with the error.
   */
  constructor(message = messages.unauthorized, data) {
    super(message, 'UnauthorizedError', 401, data);
  }
}

class ForbiddenError extends CustomError {
  /**
   * This error should be thrown whenever a client is authorized, and tries to access a resource that it is not allowed to access. Status code: 403
   * @param {String} message The error message.
   * @param {*} [data] Additional data to display with the error.
   */
  constructor(message = messages.notAllowed, data) {
    super(message, 'ForbiddenError', 403, data);
  }
}

class NotFoundError extends CustomError {
  /**
   * This error should be thrown whenever a resource cannot be found. Status code: 404
   * @param {String} message The error message.
   * @param {*} [data] Additional data to display with the error.
   */
  constructor(message = messages.notFound, data) {
    super(message, 'NotFoundError', 404, data);
  }
}

class ValidationError extends CustomError {
  /**
   * This error should be thrown whenever validation fails on the server. Status code: 422
   * @param {String} message The error message.
   * @param {*} [data] Additional data to display with the error.
   */
  constructor(message = messages.validationFailed, data) {
    super(message, 'ValidationError', 422, data);
  }
}

module.exports = {
  CustomError,
  UnauthorizedError,
  ForbiddenError,
  NotFoundError,
  ValidationError,
};
