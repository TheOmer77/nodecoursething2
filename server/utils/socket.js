const socketio = require('socket.io');

const messages = require('./messages.json');

let io;

const init = (server) => {
  io = socketio(server, {
    cors: {
      origin: 'http://localhost:3000',
      methods: ['GET', 'POST'],
    },
  });

  io.on('connection', (socket) => {
    console.log('New client connected:', socket.id);
  });
};

const getIO = () => {
  if (!io) throw new Error(messages.socketError);
  else return io;
};

const emit = (event, data) => {
  if (!io) throw new Error(messages.socketError);
  else return io.emit(event, data);
};

module.exports = { init, getIO, emit };
