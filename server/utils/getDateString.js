/**
 * Generates a string from a date including the day, month, year, hours and minutes.
 * This is neccesary, since functions like toISOString may generate a date that is invalid in a filename (on Windows).
 * @param {Date} date The date to generate a date string from.
 */
const getDateString = (date) => {
  const year = date.getFullYear();
  const month = `${date.getMonth() + 1}`.padStart(2, '0');
  const day = `${date.getDate()}`.padStart(2, '0');
  const hours = `${date.getHours()}`.padStart(2, '0');
  const minutes = `${date.getMinutes()}`.padStart(2, '0');
  return `${day}${month}${year}-${hours}${minutes}`;
};

module.exports = getDateString;
