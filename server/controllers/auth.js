const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const messages = require('../utils/messages.json');
const {
  ValidationError,
  NotFoundError,
  UnauthorizedError,
} = require('../utils/errors');
const { secret } = require('../utils/secret.json');

const User = require('../models/user');

const login = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    throw new ValidationError(errors.array()[0].msg, errors.array());
  const { email, password } = req.body;
  let currentUser;
  try {
    const user = await User.findOne({ email });
    if (!user) throw new NotFoundError(messages.emailDoesntExist);
    currentUser = user;
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) throw new UnauthorizedError(messages.passwordIncorrect);
    const token = jwt.sign(
      { email: currentUser.email, userId: currentUser._id.toString() },
      secret,
      { expiresIn: '1h' }
    );
    res.status(200).json({ token, userId: currentUser._id.toString() });
    return;
  } catch (err) {
    next(err);
    return err;
  }
};

const signUp = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    throw new ValidationError(messages.validationFailed, errors.array());
  const { email, name, password } = req.body;
  try {
    const hashedPassword = await bcrypt.hash(password, 12);
    const user = new User({
      email,
      password: hashedPassword,
      name,
    });
    const savedUser = await user.save();
    res.status(201).json({ msg: messages.userCreated, user: savedUser });
  } catch (err) {
    next(err);
  }
};

const getUserStatus = async (req, res, next) => {
  try {
    const user = await User.findById(req.userId);
    if (!user) throw new NotFoundError(messages.userNotFound);
    res.status(200).json({ status: user.status });
  } catch (err) {
    next(err);
  }
};

const updateUserStatus = async (req, res, next) => {
  const newStatus = req.body.status;
  try {
    const user = await User.findById(req.userId);
    if (!user) throw new NotFoundError(messages.userNotFound);
    user.status = newStatus;
    await user.save();
    res.status(200).json({ message: messages.userUpdated });
  } catch (err) {
    next(err);
  }
};

module.exports = { signUp, login, getUserStatus, updateUserStatus };
