const fs = require('fs');
const path = require('path');
const { validationResult } = require('express-validator');
const messages = require('../utils/messages.json');

const {
  NotFoundError,
  ValidationError,
  ForbiddenError,
} = require('../utils/errors');
const io = require('../utils/socket');

// Models
const Post = require('../models/post');
const User = require('../models/user');

// GET requests

const getPosts = async (req, res, next) => {
  const { page } = req.query || 1;
  const POSTS_PER_PAGE = 2;
  let totalItems;
  try {
    totalItems = await Post.find().countDocuments();
    const posts = await Post.find()
      .populate('creator')
      .sort({ createdAt: -1 })
      .skip((page - 1) * POSTS_PER_PAGE)
      .limit(POSTS_PER_PAGE);
    res.status(200).json({
      posts,
      totalItems,
    });
  } catch (err) {
    next(err);
  }
};

const getSinglePost = async (req, res, next) => {
  const { postId } = req.params;
  try {
    const post = await Post.findById(postId);
    if (!post) throw new NotFoundError(messages.postNotFound);
    res.status(200).json({
      post,
    });
  } catch (err) {
    next(err);
  }
};

// Other requests

const createPost = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    throw new ValidationError(messages.validationFailed, errors.array());
  else {
    const { title, content } = req.body;
    if (!req.file) throw new ValidationError(messages.imageMissing);
    /* When loading file path, replace backslash with forward slash to avoid image loading problems in client */
    const imgUrl = req.file.path.replace('\\', '/');
    const newPost = new Post({
      title,
      imgUrl,
      content,
      creator: req.userId,
    });
    try {
      await newPost.save();
      const user = await User.findById(req.userId);
      user.posts.push(newPost);
      const savedUser = await user.save();
      io.emit('createPost', {
        post: {
          ...newPost._doc,
          creator: { _id: req.userId, name: user.name },
        },
      });
      res.status(201).json({
        msg: messages.postCreated,
        post: newPost,
        creator: { _id: savedUser._id, name: savedUser.name },
      });
      return {
        msg: messages.postCreated,
        post: newPost,
        creator: savedUser,
      };
    } catch (err) {
      next(err);
    }
  }
};

const editPost = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    throw new ValidationError(messages.validationFailed, errors.array());
  else {
    const { title, content } = req.body;
    const { postId } = req.params;
    /* When loading file path, replace backslash with forward slash to avoid image loading problems in client */
    const imgUrl = req.file ? req.file.path.replace('\\', '/') : req.body.img;
    if (!imgUrl) throw new ValidationError(messages.imageMissing);
    try {
      const post = await Post.findById(postId).populate('creator');
      if (!post) throw new NotFoundError(messages.postNotFound);
      if (post.creator._id.toString() !== req.userId.toString())
        throw new ForbiddenError(messages.postEditingNotAllowed);
      if (imgUrl !== post.imgUrl) clearImage(post.imgUrl);
      post.title = title;
      post.imgUrl = imgUrl;
      post.content = content;
      const result = await post.save();
      io.emit('editPost', { post: result });
      res.status(200).json({ msg: messages.postEdited, post });
    } catch (err) {
      next(err);
    }
  }
};

const deletePost = async (req, res, next) => {
  const { postId } = req.params;
  try {
    const post = await Post.findById(postId);
    if (!post) throw new NotFoundError(messages.postNotFound);
    if (post.creator.toString() !== req.userId.toString())
      throw new ForbiddenError(messages.postDeletingNotAllowed);
    clearImage(post.imgUrl);
    await Post.findByIdAndDelete(postId);
    const user = await User.findById(req.userId);
    user.posts.pull(postId);
    await user.save();
    io.emit('deletePost', { post: postId });
    res.status(200).json({ msg: messages.postDeleted });
  } catch (err) {
    next(err);
  }
};

// Helper functions

const clearImage = (filePath) => {
  filePath = path.join(__dirname, '..', filePath);
  fs.unlink(filePath, (err) => {
    if (err) console.error(err);
  });
};

module.exports = {
  getPosts,
  getSinglePost,
  createPost,
  editPost,
  deletePost,
};
