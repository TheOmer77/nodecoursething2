const { Schema, model } = require('mongoose');
const messages = require('../utils/messages.json');

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: messages.defaultStatus,
  },
  posts: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Post',
    },
  ],
});

const userModel = model('User', userSchema);
module.exports = userModel;
