import React from 'react';

import './Auth.css';

const auth = (props) => (
  <section className='auth-form'>
    <h1>{props.title}</h1>
    {props.children}
  </section>
);

export default auth;
